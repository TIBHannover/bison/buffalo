import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/how-api',
    name: 'API',
    component: () => import('../views/Api.vue')
  },
  {
    path: '/how',
    name: 'How it works',
    component: () => import('../views/HowItWorks.vue')
  },
  {
    path: '/imprint',
    name: 'Imprint',
    component: () => import('../views/Imprint.vue')
  },
  {
    path: '/accessibility',
    name: 'Accessibility',
    component: () => import('../views/Accessibility.vue')
  },
  {
    path: '/journal/:id',
    name: 'Journal',
    component: () => import('../views/Journal.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('../views/NotFound.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    return { top: 0 }
  },
  base: '/bison/',
  routes
})

export default router
