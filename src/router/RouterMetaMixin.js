export default {
  created () {
    // set website title according to component if available
    if (this.$options.title !== undefined) {
      document.title = this.$t(this.$options.title)
    } else {
      document.title = 'B!SON'
    }
  }
}
