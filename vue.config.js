module.exports = {
  publicPath: getPublicPath(),
  configureWebpack: {
    devServer: {
      proxy: {
        '/api': {
          target: 'http://127.0.0.1:3000'
        },
        '/media': {
          target: 'http://127.0.0.1:3000'
        },
        '/static': {
          target: 'http://127.0.0.1:3000'
        }
      }
    }
  }
}

function getPublicPath() {
  if (process.env.NODE_ENV === 'docker') {
    return '/'
  } else if (process.env.VUE_APP_EXTENSION === '1') {
    return '/bison/extension/'
  } else {
    return '/bison/'
  }
}